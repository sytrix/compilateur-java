package sytrix.parser;

public interface Element {
	
	public boolean isGroup();
	public boolean isChar();
}
