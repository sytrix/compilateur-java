package sytrix.parser.langages.script;

import java.util.LinkedList;

public interface ScriptFunctionRunnable {

	public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params);
}