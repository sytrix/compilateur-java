package sytrix.parser.langages.script;


public class ScriptRange {

	private Integer start;
	private Integer end;

	public ScriptRange(Integer start, Integer end) {
		this.start = start;
		this.end = end;
	}

	public Integer getStart() {
		return this.start;
	}

	public Integer getEnd() {
		return this.end;
	}
}