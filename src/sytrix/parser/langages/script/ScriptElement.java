package sytrix.parser.langages.script;

import sytrix.parser.Element;

public enum ScriptElement implements Element {
	TokenString(false), 
	TokenAlpha(false), 
	TokenAlphaNum(false),
	TokenInteger(false), 
	TokenDouble(false),
	TokenOperatorLog(false),
	TokenOperatorCmp(false),
	TokenOperatorAS(false),
	TokenOperatorMD(false),
	TokenOperatorID(false),
	TokenKeywordIf(false),
	TokenKeywordElse(false),
	TokenKeywordFor(false),
	TokenKeywordWhile(false),
	TokenKeywordDefFunction(false),

	Character(false, true),

	GroupStart(true),
	GroupMultiInstruction(true),
	GroupInstructionClosed(true),
	GroupInstruction(true),
	GroupAffectationIndex(true),
	GroupOperatorLog(true),
	GroupOperatorCmp(true),
	GroupOperatorAS(true),
	GroupOperatorMD(true),
	GroupValueIndex(true),
	GroupIndex(true),
	GroupRange(true),
	GroupValue(true),
	GroupArray(true),
	GroupFunction(true),
	GroupList(true),
	GroupListAlpha(true),
	;

	boolean isGrp;
	boolean isChr;

	private ScriptElement(boolean isGrp) {
		this.isGrp = isGrp;
		this.isChr = false;
	}
	
	private ScriptElement(boolean isGrp, boolean isChr) {
		this.isGrp = isGrp;
		this.isChr = isChr;
	}

	public boolean isGroup() {
		return this.isGrp;
	}
	
	public boolean isChar() {
		return this.isChr;
	}


};