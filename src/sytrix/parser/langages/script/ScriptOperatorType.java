package sytrix.parser.langages.script;

public enum ScriptOperatorType {
	LowerEqual,
	GreaterEqual,
	Lower,
	Greater,
	Equal,
	Different,
	;

};