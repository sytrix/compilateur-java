package sytrix.parser.langages.script;

import java.util.Map;
import java.util.Random;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
import java.lang.Class;

public class ScriptFunction {

	private Map<String, ScriptFunctionConfig> functions;
	private static Random r = new Random();
	
	public ScriptFunction() {
		
		
		List<ScriptFunctionConfig> listConfig = new ArrayList<>();
		listConfig.add(new ScriptFunctionConfig(
			"cos",
			new Class<?>[] {Double.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d = (Double)params.get(0);
					return Math.cos(d);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"sin",
			new Class<?>[] {Double.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d = (Double)params.get(0);
					return Math.sin(d);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"tan",
			new Class<?>[] {Double.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d = (Double)params.get(0);
					return Math.tan(d);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"sqrt",
			new Class<?>[] {Double.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d = (Double)params.get(0);
					return Math.sqrt(d);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"pow",
			new Class<?>[] {Double.class, Double.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d0 = (Double)params.get(0);
					Double d1 = (Double)params.get(1);
					return Math.pow(d0, d1);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"round",
			new Class<?>[] {Double.class, Integer.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d = (Double)params.get(0);
					Integer decimal = (Integer)params.get(1);
					double dec = Math.pow(10.0, decimal);
					double res = (double)((int)(d * dec) / dec);
					return res;
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"pi",
			new Class<?>[] {},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					return Math.PI;
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"count",
			new Class<?>[] {LinkedList.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					LinkedList<?> list = (LinkedList<?>)params.get(0);
					return list.size();
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"length",
			new Class<?>[] {String.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					String str = (String)params.get(0);
					return str.length();
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"toint",
			new Class<?>[] {Double.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Double d = (Double)params.get(0);
					return new Integer((int)d.doubleValue());
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"todouble",
			new Class<?>[] {Integer.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Integer i = (Integer)params.get(0);
					return new Double((double)i.intValue());
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"tolower",
			new Class<?>[] {String.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					String s = (String)params.get(0);
					return s.toLowerCase();
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"toupper",
			new Class<?>[] {String.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					String s = (String)params.get(0);
					return s.toUpperCase();
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"isdef",
			new Class<?>[] {String.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					String s = (String)params.get(0);
					return (memory.get(s) != null) ? new Integer(1) : new Integer(0);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"isdef",
			new Class<?>[] {String.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					String s = (String)params.get(0);
					return (memory.get(s) != null) ? new Integer(1) : new Integer(0);
				}
			})
		);
		listConfig.add(new ScriptFunctionConfig(
			"rand",
			new Class<?>[] {Integer.class},
			new ScriptFunctionRunnable() {
				@Override
				public Object run(ScriptMemory memory, ScriptFunction function, LinkedList<Object> params) {
					Integer s = (Integer)params.get(0);
					return new Integer(r.nextInt(s));
				}
			})
		);
		
		this.functions = new HashMap<>();

		for(ScriptFunctionConfig config : listConfig) {
			this.functions.put(config.name, config);
		}
	}
	
	public void add(ScriptFunctionConfig fconfig) {
		this.functions.put(fconfig.name, fconfig);
	}

	public ScriptFunctionRunnable get(String name, LinkedList<Object> objects, ScriptMemory memory) {
		ScriptFunctionConfig config = functions.get(name);

		if(config != null) {
			int length = config.classes.length;
			if(objects.size() == length) {
				Iterator<Object> iteratorObj = objects.iterator();
				int i = 0;
				while(iteratorObj.hasNext()) {
					Object obj = iteratorObj.next();
					if(obj.getClass() != config.classes[i]) {
						System.err.println("erreur : parametre " + i + " type " 
							+ obj.getClass().getSimpleName() + " au lieu de " 
							+ config.classes[i].getSimpleName());
						return null;
					}

					i++;
				}

				return config.runnable;
			}

			System.err.println("erreur : nombre de parametre non correcte");
			return null;
		}
		
		Object objectFunction = memory.get(name);
		
		if (objectFunction != null) {
			if (objectFunction instanceof ScriptFunctionVariable) {
				return (ScriptFunctionVariable)objectFunction;
			}
			System.err.println("erreur : valeur n'est pas une fonction");
			return null;
		}

		System.err.println("erreur : fonction " + name + " introuvable");
		return null;
	}
}