package sytrix.parser.langages.script.test;

import java.util.HashMap;
import java.util.Map;

import sytrix.parser.Parser;
import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptParser;
import sytrix.parser.PathFindingException;
import sytrix.parser.Program;

public class TestExpressionSimple {

	static public void main(String[] args) {

		String[] listInstruction = new String[]{
			"p=5;",
			"p;",
			"f=function(well){'coucou '+well;}",
			"f('moi c\\'est pierre')",
		};

		Parser parser = new ScriptParser();
		ScriptMemory memory = new ScriptMemory(null);
		ScriptFunction functions = new ScriptFunction();
		Map<String, Object> bundle = new HashMap<>();
		bundle.put("memory", memory);
		bundle.put("functions", functions);
		
		long chronoCompilationCumul = 0;
		long chronoExecutionCumul = 0;
		long chronoStart = 0;
		long bench = 1;
		for(int i = 0; i < bench; i++) {
			for(String instruction : listInstruction) {
				chronoStart = System.currentTimeMillis();
				Program program = null;
				try {
					program = parser.compile(instruction);
				} catch (PathFindingException e) {
					System.err.println("[exception] pathfinding : unexptected token : " + e.getBadToken().toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
				chronoCompilationCumul += System.currentTimeMillis() - chronoStart;

				if(program == null) {
					System.err.println("compilation error : " + instruction);
				} else {
					chronoStart = System.currentTimeMillis();
					Object ouput = program.execute(bundle);
					chronoExecutionCumul += System.currentTimeMillis() - chronoStart;
					if(ouput == null) {
						System.out.println("null");
					} else {
						System.out.println("(" + ouput.getClass().getSimpleName() + ")\t" + ouput + "\t\t\t\t\t" + instruction + "\t\t\t\t\t\t" + program.getSource());
					}
				}
			}
		}

		System.out.println("[compilation time]:" + chronoCompilationCumul + "ms");
		System.out.println("[execution time]:" + chronoExecutionCumul + "ms");

		/*
		Program program = parser.compile("i=i+1");
		for(int i = 0; i < 10; i++) {
			Object ouput = program.execute(memory, functions);
			System.out.println("resultat(without compilation):" + ouput);
		}
		*/
	}
}