package sytrix.parser.langages.script.test;

import java.util.HashMap;
import java.util.Map;

import sytrix.parser.Parser;
import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptParser;
import sytrix.parser.PathFindingException;
import sytrix.parser.Program;

public class TestExpressionEval {

	static public void main(String[] args) {

		String[] listInstruction = new String[]{
			"m=5+6*6+2;",
			"m+5;",
			//"pierre(25,3);",
			"[5,4*9,6+3];",
			"i=0;",
			"[6,3]+5.1;",
			"5.2+[6,3];",
			"[3,4]+[1,2];",
			"cos(3.141595);",
			"3.14159-2;",
			"2-3.5;",
			"2.2-3.5;",
			"todouble(56);",
			"toint(9.36);",
			"pi();",
			"count([56.2, 52, 12, [3,5], 8.2]);",
			"\"coucou :)\";",
			"\"j'ai \" + 5.2 + \" ans\";",
			"a=\"alpha\";",
			"b=\"beta\";",
			"b + \":\" + a;",
			"tab=[45,[4,\"file\",6],22,15];",
			"tab[1][1][0];",
			"[5,6,7][2];",
			"(5+2)*4+([5,6]);",
			"toupper(\"pIérre\");",
			"tolower(\"Welc0Me\");",
			"[1,2,3,4,5,6,7,8,9,10,11,12,13,14][3:10];",
			"[];",
			"\"abcdefghijklmnopqrstuvwxyz\"[5:];",
			"pseudo=\"pierre\";",
			"toupper(pseudo[:1])+tolower(pseudo[1:]);",
			"j=5;j=j*2;j+1;",
			"cos(pi());",
			"52>=52||53<=52;",
			"53<52.9;",
			"toint(12.5)==12;",
			"14!=14;",
			"a=12;b=13;\"boolean:\"+(a>b);",
			"if(1<2){\"block true\";}", 
			"if(4<3){\"block true\";}else{\"block false\";}", 
			"for(i=0;i<10;i=i+1){m=i;}", 
			"while(i<30){m=i;i=i+1;}", 
			"i=0;while(i<30){m=m+i*2;i++;}m;", 
			"j=0;",
			"j++;",
			"j++;",
			"j++;",
			"j+=5;",
			"j*=5;",
			"j/=2;",
			"j-=7;",
			"j+=\" Coucou\"[:4];",
			"p1=12;p2=23;p1+p2;",
			"sqrt(4.0);",
			"length(pseudo);",
			"'coucou monsieur :)';",
			"3 * (('pi' * 2) + '|');",
			"round(pi(), 2);",
			"'l\\'éponge';",
			"m=3",
			"m+8",
			"p=[6,2,[5,8],[1,3],5,6]",
			"p[2][1]=18",
			"p[1]+=30",
			"p[0]*=50",
			"p",
			"add2 = function (test) {test+2;}",
			"carre = function (test) {test*test;}",
			"add2(5)",
			"carre(9)",
			"carre(2)",
			"testvarglob = function (test,p) {test+p[1];};testvarglob('coucou', [5,1.5]);",
			"p",
			
		};

		Parser parser = new ScriptParser();
		ScriptMemory memory = new ScriptMemory(null);
		ScriptFunction functions = new ScriptFunction();
		Map<String, Object> bundle = new HashMap<>();
		bundle.put("memory", memory);
		bundle.put("functions", functions);
		
		long chronoCompilationCumul = 0;
		long chronoExecutionCumul = 0;
		long chronoStart = 0;
		long bench = 1;
		for(int i = 0; i < bench; i++) {
			for(String instruction : listInstruction) {
				chronoStart = System.currentTimeMillis();
				Program program = null;
				try {
					program = parser.compile(instruction);
				} catch (PathFindingException e) {
					System.err.println("[exception] pathfinding : unexptected token : " + e.getBadToken().toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
				chronoCompilationCumul += System.currentTimeMillis() - chronoStart;

				if(program == null) {
					System.err.println("compilation error : " + instruction);
				} else {
					chronoStart = System.currentTimeMillis();
					Object ouput = program.execute(bundle);
					chronoExecutionCumul += System.currentTimeMillis() - chronoStart;
					if(ouput == null) {
						System.out.println("null");
					} else {
						System.out.println("(" + ouput.getClass().getSimpleName() + ")\t" + ouput + "\t\t\t\t\t" + instruction + "\t\t\t\t\t\t" + program.getSource());
					}
				}
			}
		}

		System.out.println("[compilation time]:" + chronoCompilationCumul + "ms");
		System.out.println("[execution time]:" + chronoExecutionCumul + "ms");
		
		System.out.println("[memory]");
		System.out.println(memory.toScript());

		/*
		Program program = parser.compile("i=i+1");
		for(int i = 0; i < 10; i++) {
			Object ouput = program.execute(memory, functions);
			System.out.println("resultat(without compilation):" + ouput);
		}
		*/
	}
}