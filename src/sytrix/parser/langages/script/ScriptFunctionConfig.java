package sytrix.parser.langages.script;

public class ScriptFunctionConfig {
	
	public ScriptFunctionRunnable runnable;
	public Class<?>[] classes;
	public String name;

	public ScriptFunctionConfig(String name, Class<?>[] classes, ScriptFunctionRunnable runnable) {
		this.name = name;
		this.classes = classes;
		this.runnable = runnable;
	}
}