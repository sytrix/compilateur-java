package sytrix.parser.langages.script;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import sytrix.parser.Instruction;


public abstract class ScriptInstruction extends Instruction {

	protected ScriptInstruction currentNextInst = this;
	
	public abstract Object nextExec(ScriptMemory memory, ScriptFunction function);
	public abstract ScriptInstruction getNextInst();
	
	public Object first(Map<String, Object> bundle) {
		Object objMemo = bundle.get("memory");
		Object objFunc = bundle.get("functions");
		
		if(objMemo == null || objFunc == null) {
			System.err.println("key 'memory' and 'functions' must be defined ");
		}
		
		ScriptMemory memory = null;
		ScriptFunction functions = null;
		
		if(objMemo instanceof ScriptMemory) {
			memory = (ScriptMemory)objMemo;
		} else {
			System.err.println("incorrect 'memory' object : " + objMemo.toString());
		}
		if(objFunc instanceof ScriptFunction) {
			functions = (ScriptFunction)objFunc;
		} else {
			System.err.println("incorrect 'functions' object : " + objFunc.toString());
		}
		
		if(objMemo == null || objFunc == null) {
			return null;
		}
		
		return this.exec(memory, functions);
	}
	
	public Object exec(ScriptMemory memory, ScriptFunction function) {
		return currentNextInst.nextExec(memory, function);
	}
	
	public void makeFaster() {
		currentNextInst = this.getNextInst();
	}
	
	public ScriptInstruction getCurrentNextInst() {
		return currentNextInst;
	}

	protected boolean isNumber(Object obj) {
		if(obj instanceof Double || obj instanceof Integer) {
			return true;
		}

		return false;
	}
	
	protected Object recuperation(LinkedList<ScriptRange> listIndex, Object value) {
		Iterator<ScriptRange> iteratorIndex = listIndex.iterator();

		while(iteratorIndex.hasNext()) {
			ScriptRange range = iteratorIndex.next();
			Integer startIndex = range.getStart();
			Integer endIndex = range.getEnd();

			if(endIndex == null) {
				if(value instanceof LinkedList<?>) {
					LinkedList<Object> valueLinkedList = (LinkedList<Object>)value;
					if(startIndex >= 0 && startIndex < valueLinkedList.size()) {
						value = valueLinkedList.get(startIndex);
					} else {
						System.err.println("erreur : index out of bound");
						return null;
					}
				} else if(value instanceof String) {
					String valueString = (String)value;
					if(startIndex >= 0 && startIndex < valueString.length()) {
						value = valueString.substring(startIndex, startIndex + 1);
					} else {
						System.err.println("erreur : index out of bound");
						return null;
					}
				} else {
					System.err.println("erreur : operation accès à l'index impossible sur ce type de valeur");
					return null;
				}
			} else {
				if(startIndex == Integer.MAX_VALUE) {
					startIndex = 0;
				}
				if(startIndex > endIndex) {
					System.err.println("erreur : range start > range end");	
					return null;
				}

				if(value instanceof LinkedList<?>) {
					LinkedList<Object> valueLinkedList = (LinkedList<Object>)value;
					int listSize = valueLinkedList.size();

					if(endIndex == Integer.MAX_VALUE) {
						endIndex = listSize;
					}

					if(startIndex >= 0 && startIndex <= listSize
						&& endIndex >= 0 && endIndex <= listSize) {

						value = valueLinkedList.subList(startIndex, endIndex);
					} else {
						System.err.println("erreur : index out of bound");
						return null;
					}
				} else if(value instanceof String) {
					String valueString = (String)value;
					int stringLength = valueString.length();

					if(endIndex == Integer.MAX_VALUE) {
						endIndex = stringLength;
					}

					if(startIndex >= 0 && startIndex <= stringLength
						&& endIndex >= 0 && endIndex <= stringLength) {

						value = valueString.substring(startIndex, endIndex);						
					} else {
						System.err.println("erreur : index out of bound");
						return null;
					}
				} else {
					System.err.println("erreur : operation accès à l'index impossible sur ce type de valeur");
					return null;
				}
			}
			
		}
		
		return value;
	}
	
	@SuppressWarnings("unchecked")
	protected Object affectation(ScriptMemory memory, String variable, LinkedList<ScriptRange> listIndex, Object value) {
		
		if(listIndex.isEmpty()) {
			memory.put(variable, value);
		} else {
			Iterator<ScriptRange> ranges = listIndex.iterator();
			Object obj = memory.get(variable);
			
			while(ranges.hasNext()) {
				ScriptRange r = ranges.next();
				
				if(r.getEnd() != null) {
					System.err.println("erreur : affectation à une range impossible");
					return null;
				}
				
				int index = r.getStart();
				
				if(obj == null) {
					System.err.println("erreur : impossible l'objet n'est pas défini");
					return null;
				}
				
				if(obj instanceof LinkedList) {
					LinkedList<Object> valueLinkedList = (LinkedList<Object>)obj;
					
					if(index < 0 || index >= valueLinkedList.size()) {
						System.err.println("erreur : index out of bound");
						return null;
					}
					
					if(ranges.hasNext()) {
						obj = valueLinkedList.get(index);
					} else {
						valueLinkedList.set(index, value);
					}
					
				} else {
					
					System.err.println("erreur : impossible de modifier l'index d'un type " + obj.getClass().getSimpleName());
					return null;
				}
			}
		}
		
		return value;
	}

	@SuppressWarnings("unchecked")
	protected Object operationAS(Object value1, char operator, Object value2) {

		if(value1 instanceof Integer && value2 instanceof Integer) {
			Integer i1 = (Integer)value1;
			Integer i2 = (Integer)value2;

			if(operator == '+') {
				return new Integer(i1 + i2);
			} else if(operator == '-') {
				return new Integer(i1 - i2);
			}
		} 

		if(value1 instanceof Double && value2 instanceof Integer) {
			Double d1 = (Double)value1;
			Integer i2 = (Integer)value2;

			if(operator == '+') {
				return new Double(d1 + i2);
			} else if(operator == '-') {
				return new Double(d1 - i2);
			}
		} 

		if(value1 instanceof Double && value2 instanceof Double) {
			Double d1 = (Double)value1;
			Double d2 = (Double)value2;

			if(operator == '+') {
				return new Double(d1 + d2);
			} else if(operator == '-') {
				return new Double(d1 - d2);
			}
		}

		if(value1 instanceof Integer && value2 instanceof Double) {
			Integer i1 = (Integer)value1;
			Double d2 = (Double)value2;

			if(operator == '+') {
				return new Double(i1 + d2);
			} else if(operator == '-') {
				return new Double(i1 - d2);
			}
		}

		if(value1 instanceof LinkedList && this.isNumber(value2)) {
			if(operator == '+') {
				LinkedList<Object> l1 = (LinkedList<Object>)value1;
				l1.add(value2);
				return l1;
			}
		}

		if(this.isNumber(value1) && value2 instanceof LinkedList) {
			if(operator == '+') {
				LinkedList<Object> l2 = (LinkedList<Object>)value2;
				l2.push(value1);
				return l2;
			}
		}

		if(value1 instanceof LinkedList && value2 instanceof LinkedList) {
			if(operator == '+') {
				LinkedList<Object> l1 = (LinkedList<Object>)value1;
				LinkedList<Object> l2 = (LinkedList<Object>)value2;
				l1.addAll(l2);
				return l1;
			}
		}

		if(value1 instanceof String) {
			if(operator == '+') {
				return value1.toString() + value2.toString();
			}
		}

		if(value2 instanceof String) {
			if(operator == '+') {
				return value1.toString() + value2.toString();
			}
		}

		System.err.println("operation '" + operator + "' impossible sur type : " + 
			value1.getClass().getSimpleName() + " - " + value2.getClass().getSimpleName());
		return null;
	}

	protected Object operationMD(Object value1, char operator, Object value2) {
		if(value1 instanceof Integer && value2 instanceof Integer) {
			Integer i1 = (Integer)value1;
			Integer i2 = (Integer)value2;

			if(operator == '*') {
				return new Integer(i1 * i2);
			} else if(operator == '/') {
				return new Integer(i1 / i2);
			}
		} 

		if(value1 instanceof Double && value2 instanceof Integer) {
			Double d1 = (Double)value1;
			Integer i2 = (Integer)value2;

			if(operator == '*') {
				return new Double(d1 * i2);
			} else if(operator == '/') {
				return new Double(d1 / i2);
			}
		} 

		if(value1 instanceof Double && value2 instanceof Double) {
			Double d1 = (Double)value1;
			Double d2 = (Double)value2;

			if(operator == '*') {
				return new Double(d1 * d2);
			} else if(operator == '/') {
				return new Double(d1 / d2);
			}
		}

		if(value1 instanceof Integer && value2 instanceof Double) {
			Integer i1 = (Integer)value1;
			Double d2 = (Double)value2;

			if(operator == '*') {
				return new Double(i1 * d2);
			} else if(operator == '/') {
				return new Double(i1 / d2);
			}
		}
		
		if(value1 instanceof Integer && value2 instanceof String) {
			Integer i1 = (Integer)value1;
			String s2 = (String)value2;
			StringBuilder res = new StringBuilder();

			if(operator == '*') {
				for(int i = 0; i < i1; i++) {
					res.append(s2);
				}
				
				return res.toString();
			}
		}
		
		if(value1 instanceof String && value2 instanceof Integer) {
			String s1 = (String)value1;
			Integer i2 = (Integer)value2;
			StringBuilder res = new StringBuilder();

			if(operator == '*') {
				for(int i = 0; i < i2; i++) {
					res.append(s1);
				}
				
				return res.toString();
			}
		}

		System.err.println("operation '" + operator + "' impossible sur type : " + 
			value1.getClass().getSimpleName() + " - " + value2.getClass().getSimpleName());
		return null;
	}
}