package sytrix.parser.langages.script;

import java.util.Iterator;
import java.util.LinkedList;

import sytrix.parser.langages.script.instruction.InstructionMultiple;

public class ScriptFunctionVariable implements ScriptFunctionRunnable {

	private LinkedList<String> paramsName;
	private InstructionMultiple instructionMultiple;
	
	public ScriptFunctionVariable(LinkedList<String> listParam, InstructionMultiple instructionMultiple) {
		this.paramsName = listParam;
		this.instructionMultiple = instructionMultiple;
	}
	
	@Override
	public Object run(ScriptMemory parentScope, ScriptFunction function, LinkedList<Object> values) {
		
		if(values.size() != this.paramsName.size()) {
			
			System.err.println("erreur : nombre de parametre non correcte");
			return null;
		}
		
		ScriptMemory childScope = new ScriptMemory(parentScope);
		Iterator<String> iterNames = this.paramsName.iterator();
		Iterator<Object> iterValues = values.iterator();
		
		while(iterNames.hasNext()) {
			String name = iterNames.next();
			Object value = iterValues.next();
			
			//System.out.println("DEBUG:" + name + "-" + value);
			
			childScope.putFirstPlace(name, value);
		}
		
		return instructionMultiple.exec(childScope, function);
	}
	
	public String source() {
		Iterator<String> iter = paramsName.iterator();
		StringBuilder params = new StringBuilder();
		
		if(iter.hasNext()) {
			params.append(iter.next());
		}
		
		while(iter.hasNext()) {
			params.append(",");
			params.append(iter.next());
		}
		
		return "function(" + params + "){" + this.instructionMultiple.source() + "}";
	}
	
	public void makeFaster() {
		instructionMultiple.makeFaster();
	}
	
	@Override
	public String toString() {
		return "fun(" + this.paramsName.toString() + "){" + instructionMultiple.source() + "}";
	}

}
