package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueArray extends InstructionValue {

	private InstructionList instructionList;

	public InstructionValueArray(InstructionList instructionList) {
		this.instructionList = instructionList;
	}

	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {

		LinkedList<Object> values = null;

		if(this.instructionList == null) {
			values = new LinkedList<>();
		} else {
			values = (LinkedList<Object>)this.instructionList.exec(memory, function);
		}
		
		return values;
	}

	@Override
	public String source() {
		if(this.instructionList == null) {
			return "[]";
		}
		
		return "[" + this.instructionList.source() + "]";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		if(this.instructionList != null) {
			this.instructionList.makeFaster();
		}
		return this;
	}

}