package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionOperationMD extends ScriptInstruction {

	private InstructionValue value;
	private InstructionOperationMD next;
	private char operator;

	public InstructionOperationMD(InstructionValue value, Character operator, InstructionOperationMD next) {
		this.value = value;
		this.next = next;
		this.operator = '?';
		
		if(operator != null) {
			this.operator = operator.charValue();
		}
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object value1 = this.value.exec(memory, function);

		if(this.next == null) {
			return value1;
		}
		
		if(value1 == null) {
			return null;
		}

		Object value2 = this.next.exec(memory, function);

		return this.operationMD(value1, operator, value2);
	}
	
	@Override
	public String source() {
		if(this.next == null) {
			return this.value.source();
		}
		
		return this.value.source() + this.operator + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.value.makeFaster();
		if(this.next == null) {
			return this.value.getNextInst();
		}
		
		this.next.makeFaster();
		return this;
	}
}