package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueString extends InstructionValue {


	private String string;

	public InstructionValueString(String string) {
		this.string = string;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		return this.string;
	}

	@Override
	public String source() {
		return "'" + string.replace("\\", "\\\\").replace("'", "\\'") + "'";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		return this;
	}
}