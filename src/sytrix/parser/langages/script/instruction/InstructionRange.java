package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptRange;

public class InstructionRange extends ScriptInstruction {

	private InstructionOperationLog start;
	private InstructionOperationLog end;
	private boolean startEmpty;
	private boolean endEmpty;

	public InstructionRange(InstructionOperationLog start, InstructionOperationLog end, boolean startEmpty, boolean endEmpty) {
		this.start = start;
		this.end = end;
		this.startEmpty = startEmpty;
		this.endEmpty = endEmpty;
	}

	
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {

		Object startValue = null;
		Object endValue = null;

		Integer startInt = null;
		Integer endInt = null;

		if(this.startEmpty) {
			startInt = Integer.MAX_VALUE;
		} else {
			startValue = this.start.exec(memory, function);
			
			if(startValue == null) {
				return null;
			}

			if(startValue instanceof Integer) {
				startInt = (Integer)startValue;
			} else {
				System.err.println("erreur : les index doivent être des entiers (range start)");
				return null;
			}
		}

		if(this.endEmpty) {
			endInt = Integer.MAX_VALUE;
		} else {
			if(this.end != null) {
				endValue = this.end.exec(memory, function);
				
				if(endValue == null) {
					return null;
				}

				if(endValue instanceof Integer) {
					endInt = (Integer)endValue;
				} else {
					System.err.println("erreur : les index doivent être des entiers (range end)");
					return null;
				}
			}
		}

		return new ScriptRange(startInt, endInt);
	}


	public String source() {
		 
		if(startEmpty) {
			if(endEmpty) {
				return "[:]";
			} else {
				return "[:" + this.end.source() + "]";
			}
		} else {
			if(endEmpty) {
				return "[" + this.start.source() + ":]";
			} else {
				if(this.end == null) {
					return "[" + this.start.source() + "]";
				}
				
				return "[" + this.start.source() + ":" + this.end.source() + "]";
			}
		}
	}

	@Override
	public ScriptInstruction getNextInst() {
		if(this.start != null) {
			this.start.makeFaster();
		}
		if(this.end != null) {
			this.end.makeFaster();
		}
		return this;
	}	
}
	
	