package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptFunctionVariable;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionKeywordDefFunction extends InstructionValue {

	private InstructionListAlpha listAlpha;
	private InstructionMultiple instructionMultiple;
	
	public InstructionKeywordDefFunction(InstructionListAlpha listAlpha, InstructionMultiple instructionMultiple) {
		this.listAlpha = listAlpha;
		this.instructionMultiple = instructionMultiple;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {

		LinkedList<String> listParam = (LinkedList<String>)listAlpha.exec(memory, function);
		
		if(listParam == null) {
			return null;
		}
		
		return new ScriptFunctionVariable(listParam, instructionMultiple);
	}

	@Override
	public String source() {
		return "function(" + this.listAlpha.source() + "){" + this.instructionMultiple.source() + "}";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.listAlpha.makeFaster();
		this.instructionMultiple.makeFaster();
		return this;
	}
	

}
