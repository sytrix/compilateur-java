package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueVariableID extends InstructionValue {

	private String variable;
	private char operator;

	public InstructionValueVariableID(String variable, Character operator) {
		this.variable = variable;
		this.operator = operator.charValue();
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		Object value = memory.get(this.variable);

		if(value != null) {
			if(value instanceof Integer) {
				Integer valueInt = (Integer)value;

				if(this.operator == '+') {
					valueInt++;
				} else if(this.operator == '-') {
					valueInt--;
				}

				memory.put(this.variable, valueInt);


				return valueInt;
			} else if(value instanceof Double) {
				Double valueDouble = (Double)value;

				if(this.operator == '+') {
					valueDouble++;
				} else if(this.operator == '-') {
					valueDouble--;
				}

				memory.put(this.variable, valueDouble);

				return valueDouble;
			}
		}

		return null;
	}

	@Override
	public String source() {
		return this.variable + this.operator + this.operator;
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		return this;
	}
}