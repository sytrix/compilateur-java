package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptRange;

public class InstructionAffectationIndex extends ScriptInstruction {

	private String variable;
	private InstructionIndex instructionIndex;
	
	public InstructionAffectationIndex(String variable, InstructionIndex instructionIndex) {
		this.variable = variable;
		this.instructionIndex = instructionIndex;
	}

	@Override
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		if(this.instructionIndex == null) {
			return new LinkedList<ScriptRange>();
		}
		
		return instructionIndex.exec(memory, function);
	}

	public String getVariable() {
		return variable;
	}
	
	@Override
	public String source() {
		if(this.instructionIndex == null) {
			return variable;
		}
		
		return variable + this.instructionIndex.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		if(this.instructionIndex != null) {
			this.instructionIndex.makeFaster();
		}
		return this;
	}

}
