package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptRange;

public class InstructionAffectationAS extends ScriptInstruction {

	private InstructionAffectationIndex affectationIndex;
	private InstructionOperationLog operation;
	private char operator;

	public InstructionAffectationAS(InstructionAffectationIndex affectationIndex, Character operator, InstructionOperationLog operation) {
		this.affectationIndex = affectationIndex;
		this.operation = operation;
		this.operator = operator;
	}

	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		LinkedList<ScriptRange> listIndex = (LinkedList<ScriptRange>)this.affectationIndex.exec(memory, function);
		
		if(listIndex == null) {
			return null;
		}
		
		String variable = this.affectationIndex.getVariable();
		Object value = memory.get(variable);
		value = this.recuperation(listIndex, value);

		if(value == null) {
			return null;
		}

		Object valueOperation = this.operation.exec(memory, function);

		if(valueOperation == null) {
			return null;
		}

		value = this.operationAS(value, this.operator, valueOperation);

		if(value == null) {
			return null;
		}
		
		
		
		return this.affectation(memory, variable, listIndex, value);
	}
	
	@Override
	public String source() {
		return this.affectationIndex.source() + this.operator + "=" + this.operation.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.affectationIndex.makeFaster();
		this.operation.makeFaster();
		return this;
	}

}