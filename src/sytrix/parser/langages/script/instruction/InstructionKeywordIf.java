package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionKeywordIf extends ScriptInstruction {

	private InstructionOperationLog condition;
	private InstructionMultiple blockTrue;
	private InstructionMultiple blockFalse;

	public InstructionKeywordIf(InstructionOperationLog condition, InstructionMultiple blockTrue, InstructionMultiple blockFalse) {
		this.condition = condition;
		this.blockTrue = blockTrue;
		this.blockFalse = blockFalse;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object conditionValue = this.condition.exec(memory, function);

		if(conditionValue == null) {
			return null;
		}

		Object value = conditionValue;

		if(conditionValue instanceof Integer) {
			Integer conditionInt = (Integer)conditionValue;
			if(conditionInt.intValue() > 0) {
				value = this.blockTrue.exec(memory, function);
			} else {
				if(this.blockFalse != null){
					value = this.blockFalse.exec(memory, function);
				}
			}
		}

		return value;
	}

	@Override
	public String source() {
		if(this.blockFalse == null) {
			return "if(" + this.condition.source() + "){" + this.blockTrue.source() + "}";
		}
		return "if(" + this.condition.source() + "){" + this.blockTrue.source() + "}else{" + this.blockFalse.source() + "}";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.condition.makeFaster();
		this.blockTrue.makeFaster();
		if(this.blockFalse != null) {
			this.blockFalse.makeFaster();
		}
		return this;
	}
}

