package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueVariable extends InstructionValue {


	private String variable;

	public InstructionValueVariable(String variable) {
		this.variable = variable;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		return memory.get(this.variable);
	}

	@Override
	public String source() {
		return variable;
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		return this;
	}
}