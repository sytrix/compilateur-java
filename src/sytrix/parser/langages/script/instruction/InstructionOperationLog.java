package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionOperationLog extends ScriptInstruction {

	private InstructionOperationCmp current;
	private Character operator;
	private InstructionOperationLog next;

	public InstructionOperationLog(InstructionOperationCmp current, Character operator, InstructionOperationLog next) {
		this.current = current;
		this.operator = operator;
		this.next = next;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object value1 = this.current.exec(memory, function);

		if(this.next == null) {
			return value1;
		}
		
		if(value1 == null) {
			return null;
		}

		Object value2 = this.next.exec(memory, function);

		if(value2 == null) {
			return null;
		}

		if(value1 instanceof Integer && value2 instanceof Integer) {
			Integer i1 = (Integer)value1;
			Integer i2 = (Integer)value2;

			if(this.operator.charValue() == '&') {
				return new Integer((i1>=1 && i2>=1) ? 1:0);
			} else if(this.operator.charValue() == '|') {
				return new Integer((i1>=1 || i2>=1) ? 1:0);
			} 
		} 

		System.err.println("type incompatible pour comparaison");
		return null;
	}

	@Override
	public String source() {
		if(this.next == null) {
			return this.current.source();
		}
		
		return this.current.source() + this.operator + this.operator + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.current.makeFaster();
		if(this.next == null) {
			return this.current.getNextInst();
		}
		
		this.next.makeFaster();
		return this;
	}
}



