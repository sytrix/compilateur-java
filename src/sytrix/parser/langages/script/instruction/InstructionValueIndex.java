package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptRange;

public class InstructionValueIndex extends InstructionValue {

	private InstructionValue instructionValue;
	private InstructionIndex instructionIndex;

	public InstructionValueIndex(InstructionValue instructionValue, InstructionIndex instructionIndex) {
		this.instructionValue = instructionValue;
		this.instructionIndex = instructionIndex;
	}

	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		Object value = instructionValue.exec(memory, function);

		if(this.instructionIndex == null) {
			return value;
		}

		if(value == null) {
			return null;
		}

		LinkedList<ScriptRange> listIndex = (LinkedList<ScriptRange>)instructionIndex.exec(memory, function);
		
		return this.recuperation(listIndex, value);
	}
	
	@Override
	public String source() {
		if(this.instructionIndex == null) {
			//System.err.println(this.instructionValue.getClass());
			return this.instructionValue.source();
		}
		
		return this.instructionValue.source() + this.instructionIndex.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.instructionValue.makeFaster();
		if(this.instructionIndex == null) {
			return this.instructionValue.getNextInst();
		}
		this.instructionIndex.makeFaster();
		return this;
	}

}


