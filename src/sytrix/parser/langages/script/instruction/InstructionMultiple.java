package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionMultiple extends ScriptInstruction {

	private ScriptInstruction current;
	private InstructionMultiple next;

	public InstructionMultiple(ScriptInstruction current, InstructionMultiple next) {
		this.current = current;
		this.next = next;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object value = this.current.exec(memory, function);

		if(value == null) {
			return null;
		}

		if(this.next != null) {
			value = this.next.exec(memory, function);
		}

		return value;
	}

	@Override
	public String source() {
		if(this.next == null) {
			return this.current.source() + ";";
		}
		
		return this.current.source() + ";" + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.current.makeFaster();
		if(this.next == null) {
			return this.current.getNextInst();
		}
		
		this.next.makeFaster();
		return this;
	}

}