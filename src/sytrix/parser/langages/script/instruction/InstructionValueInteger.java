package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueInteger extends InstructionValue {

	private Integer integer;

	public InstructionValueInteger(Integer integer) {
		this.integer = integer;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		return this.integer;
	}

	@Override
	public String source() {
		return integer.toString();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		return this;
	}
}