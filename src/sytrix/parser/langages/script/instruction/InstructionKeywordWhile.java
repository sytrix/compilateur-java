package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionKeywordWhile extends ScriptInstruction {

	private InstructionOperationLog opCondition;
	private InstructionMultiple blockToRepeat;

	public InstructionKeywordWhile(InstructionOperationLog opCondition, InstructionMultiple blockToRepeat) {
		this.opCondition = opCondition;
		this.blockToRepeat = blockToRepeat;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		
		Object conditionValue = null;
		Integer conditionInt = null;
		Object returnValue = new Integer(1);

		do {
			conditionValue = this.opCondition.exec(memory, function);

			if(conditionValue == null) {
				return null;
			}

			if(conditionValue instanceof Integer) {
				conditionInt = (Integer)conditionValue;
				if(conditionInt.intValue() <= 0) {
					return returnValue;
				}
			} else {
				System.err.println("erreur la condition d'une boucle while doit renvoyé un nombre entier (boolean)");
				return null;
			}

			returnValue = this.blockToRepeat.exec(memory, function);

			if(returnValue == null) {
				return null;
			}

		} while(true);
	}
	
	@Override
	public String source() {
		return "while(" + this.opCondition.source() + "){" + this.blockToRepeat.source() + "}";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.opCondition.makeFaster();
		this.blockToRepeat.makeFaster();
		return this;
	}

}

