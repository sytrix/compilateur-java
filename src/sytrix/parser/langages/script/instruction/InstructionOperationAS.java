package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionOperationAS extends ScriptInstruction {

	private InstructionOperationMD current;
	private InstructionOperationAS next;
	private char operator;

	public InstructionOperationAS(InstructionOperationMD current, Character operator, InstructionOperationAS next) {
		this.current = current;
		this.next = next;
		this.operator = '?';
		
		if(operator != null) {
			this.operator = operator.charValue();
		}
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object value1 = this.current.exec(memory, function);

		if(this.next == null) {
			return value1;
		}
		
		if(value1 == null) {
			return null;
		}

		Object value2 = this.next.exec(memory, function);

		if(value2 == null) {
			return null;
		}

		return this.operationAS(value1, this.operator, value2);
	}
	
	@Override
	public String source() {
		if(this.next == null) {
			return this.current.source();
		}
		
		return this.current.source() + this.operator + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.current.makeFaster();
		if(this.next == null) {
			return this.current.getNextInst();
		}
		
		this.next.makeFaster();
		return this;
	}
}