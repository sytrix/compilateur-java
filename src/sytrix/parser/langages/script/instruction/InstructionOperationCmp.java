package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptOperatorType;

public class InstructionOperationCmp extends ScriptInstruction {

	private InstructionOperationAS current;
	private ScriptOperatorType operator;
	private InstructionOperationCmp next;

	public InstructionOperationCmp(InstructionOperationAS current, ScriptOperatorType operator, InstructionOperationCmp next) {
		this.current = current;
		this.operator = operator;
		this.next = next;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object value1 = this.current.exec(memory, function);

		if(this.next == null) {
			return value1;
		}
		
		if(value1 == null) {
			return null;
		}

		Object value2 = this.next.exec(memory, function);

		if(value2 == null) {
			return null;
		}

		if(value1 instanceof Integer && value2 instanceof Integer) {
			int i1 = ((Integer)value1).intValue();
			int i2 = ((Integer)value2).intValue();

			if(this.operator == ScriptOperatorType.LowerEqual) {
				return new Integer((i1 <= i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.GreaterEqual) {
				return new Integer((i1 >= i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Lower) {
				return new Integer((i1 < i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Greater) {
				return new Integer((i1 > i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Equal) {
				return new Integer((i1 == i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Different) {
				return new Integer((i1 != i2) ? 1:0);
			} 
		} 

		if(value1 instanceof Double && value2 instanceof Integer) {
			double d1 = ((Double)value1).doubleValue();
			double i2 = (double)((Integer)value2).intValue();

			if(this.operator == ScriptOperatorType.LowerEqual) {
				return new Integer((d1 <= i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.GreaterEqual) {
				return new Integer((d1 >= i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Lower) {
				return new Integer((d1 < i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Greater) {
				return new Integer((d1 > i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Equal) {
				return new Integer((d1 == i2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Different) {
				return new Integer((d1 != i2) ? 1:0);
			} 
		} 

		if(value1 instanceof Double && value2 instanceof Double) {
			double d1 = ((Double)value1).doubleValue();
			double d2 = ((Double)value2).doubleValue();

			if(this.operator == ScriptOperatorType.LowerEqual) {
				return new Integer((d1 <= d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.GreaterEqual) {
				return new Integer((d1 >= d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Lower) {
				return new Integer((d1 < d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Greater) {
				return new Integer((d1 > d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Equal) {
				return new Integer((d1 == d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Different) {
				return new Integer((d1 != d2) ? 1:0);
			} 
		}

		if(value1 instanceof Integer && value2 instanceof Double) {
			double i1 = (double)((Integer)value1).intValue();
			double d2 = ((Double)value2).doubleValue();

			if(this.operator == ScriptOperatorType.LowerEqual) {
				return new Integer((i1 <= d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.GreaterEqual) {
				return new Integer((i1 >= d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Lower) {
				return new Integer((i1 < d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Greater) {
				return new Integer((i1 > d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Equal) {
				return new Integer((i1 == d2) ? 1:0);
			} else if(this.operator == ScriptOperatorType.Different) {
				return new Integer((i1 != d2) ? 1:0);
			} 
		}

		System.err.println("type incompatible pour comparaison");
		return null;
	}
	
	@Override
	public String source() {
		if(this.next == null) {
			return this.current.source();
		}
		
		String operatorStr = "??";
		
		if(this.operator == ScriptOperatorType.LowerEqual) {
			operatorStr = "<=";
		} else if(this.operator == ScriptOperatorType.GreaterEqual) {
			operatorStr = ">=";
		} else if(this.operator == ScriptOperatorType.Lower) {
			operatorStr = "<";
		} else if(this.operator == ScriptOperatorType.Greater) {
			operatorStr = ">";
		} else if(this.operator == ScriptOperatorType.Equal) {
			operatorStr = "==";
		} else if(this.operator == ScriptOperatorType.Different) {
			operatorStr = "!=";
		} 
		
		return this.current.source() + operatorStr + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.current.makeFaster();
		if(this.next == null) {
			return this.current.getNextInst();
		}
		
		this.next.makeFaster();
		return this;
	}
}