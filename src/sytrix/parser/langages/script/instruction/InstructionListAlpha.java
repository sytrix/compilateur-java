package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionListAlpha extends ScriptInstruction {

	private String current;
	private InstructionListAlpha next;
	
	public InstructionListAlpha(String current, InstructionListAlpha next) {
		this.current = current;
		this.next = next;
	}
	
	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		LinkedList<String> linkedList = null;

		if(this.next == null) {
			linkedList = new LinkedList<>();
		} else {
			linkedList = (LinkedList<String>)this.next.exec(memory, function);
		}

		if(linkedList != null) {
			linkedList.push(this.current);
		}

		return linkedList;
	}
	
	public String source() {
		if(this.next == null) {
			return this.current;
		}
		
		return this.current + "," + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		if(this.next != null) {
			this.next.makeFaster();
		}
		return this;
	}

}
