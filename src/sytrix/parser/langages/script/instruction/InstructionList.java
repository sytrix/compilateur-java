package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionList extends ScriptInstruction {

	// alpha,beta,gamma
	private InstructionOperationLog current;
	private InstructionList next;

	public InstructionList(InstructionOperationLog current, InstructionList next) {
		this.current = current;
		this.next = next;
	}

	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {

		LinkedList<Object> linkedList = null;

		if(this.next == null) {
			linkedList = new LinkedList<>();
		} else {
			linkedList = (LinkedList<Object>)this.next.exec(memory, function);
		}

		if(linkedList != null) {
			Object value = this.current.exec(memory, function);

			if(value == null) {
				return null;
			} else {
				linkedList.push(value);
			}
		}

		return linkedList;
	}

	public String source() {
		if(this.next == null) {
			return this.current.source();
		}
		
		return this.current.source() + "," + this.next.source();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.current.makeFaster();
		if(this.next != null) {
			this.next.makeFaster();
		}
		
		return this;
	}	
}