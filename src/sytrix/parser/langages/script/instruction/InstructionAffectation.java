package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;
import sytrix.parser.langages.script.ScriptRange;

public class InstructionAffectation extends ScriptInstruction {

	private InstructionAffectationIndex affectationIndex;
	private InstructionOperationLog operation;

	public InstructionAffectation(InstructionAffectationIndex affectationIndex, InstructionOperationLog operation) {
		this.affectationIndex = affectationIndex;
		this.operation = operation;
	}

	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		LinkedList<ScriptRange> listIndex = (LinkedList<ScriptRange>)this.affectationIndex.exec(memory, function);
		
		if(listIndex == null) {
			return null;
		}
		
		Object value = this.operation.exec(memory, function);
		
		if(value == null) {
			return null;
		}
		
		String variable = this.affectationIndex.getVariable();
		
		return this.affectation(memory, variable, listIndex, value);

	}

	@Override
	public String source() {
		return this.affectationIndex.source() + "=" + this.operation.source();
	}

	@Override
	public ScriptInstruction getNextInst() {
		affectationIndex.makeFaster();
		operation.makeFaster();
		return this;
	}

}