package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionKeywordFor extends ScriptInstruction {

	private ScriptInstruction opInit;
	private InstructionOperationLog opCondition;
	private ScriptInstruction opIncrement;
	private InstructionMultiple blockToRepeat;

	public InstructionKeywordFor(
		ScriptInstruction opInit, 
		InstructionOperationLog opCondition, 
		ScriptInstruction opIncrement, 
		InstructionMultiple blockToRepeat) {
		this.opInit = opInit;
		this.opCondition = opCondition;
		this.opIncrement = opIncrement;
		this.blockToRepeat = blockToRepeat;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		
		Object initValue = this.opInit.exec(memory, function);
		
		if(initValue == null) {
			return null;
		}

		Object conditionValue = null;
		Integer conditionInt = null;
		Object returnValue = new Integer(1);
		
		ScriptInstruction sCond = this.opCondition.getCurrentNextInst();
		ScriptInstruction sIncr = this.opIncrement.getCurrentNextInst();
		ScriptInstruction sRepeat = this.blockToRepeat.getCurrentNextInst();
		
		System.out.println(sCond.getClass());
		System.out.println(sIncr.getClass());
		System.out.println(sRepeat.getClass());

		do {
			//System.out.println("-condition");
			
			conditionValue = sCond.nextExec(memory, function);

			if(conditionValue == null) {
				return null;
			}
			
			if(conditionValue instanceof Integer) {
				conditionInt = (Integer)conditionValue;
				if(conditionInt.intValue() <= 0) {
					return returnValue;
				}
			} else {
				System.err.println("erreur la condition d'une boucle for doit renvoyé un nombre entier (boolean)");
				return null;
			}
			
			//System.out.println("-block");

			returnValue = sRepeat.nextExec(memory, function);

			if(returnValue == null) {
				return null;
			}
			
			//System.out.println("-increment");

			Object incrementValue = sIncr.nextExec(memory, function);

			if(incrementValue == null) {
				return null;
			}
			

		} while(true);
	}

	@Override
	public String source() {
		return "for(" + this.opInit.source() + ";" + this.opCondition.source() + ";" + this.opIncrement.source() + "){" + this.blockToRepeat.source() + "}";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.opInit.makeFaster();
		this.opCondition.makeFaster();
		this.opIncrement.makeFaster();
		this.blockToRepeat.makeFaster();
		return this;
	}
	

}

