package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueDouble extends InstructionValue {

	private Double d;

	public InstructionValueDouble(Double d) {
		this.d = d;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		return this.d;
	}
	
	@Override
	public String source() {
		return d.toString();
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		return this;
	}
}