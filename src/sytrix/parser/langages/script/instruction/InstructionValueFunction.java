package sytrix.parser.langages.script.instruction;

import java.util.LinkedList;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptFunctionRunnable;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueFunction extends InstructionValue {

	private String functionName;
	private InstructionList instructionList;

	public InstructionValueFunction(String functionName, InstructionList instructionList) {
		this.functionName = functionName;
		this.instructionList = instructionList;
	}

	@SuppressWarnings("unchecked")
	public Object nextExec(ScriptMemory memory, ScriptFunction function) {

		LinkedList<Object> values = null;

		if(this.instructionList == null) {
			values = new LinkedList<>();
		} else {
			values = (LinkedList<Object>)this.instructionList.exec(memory, function);
		}

		if(values != null) {

			ScriptFunctionRunnable functionRunnable = function.get(this.functionName, values, memory);

			if(functionRunnable == null) {
				return null;
			}

			return functionRunnable.run(memory, function, values);
		}
		
		return null;
	}

	@Override
	public String source() {
		if(this.instructionList == null) {
			return this.functionName + "()";
		}
		
		return this.functionName + "(" + this.instructionList.source() + ")";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		if(this.instructionList != null) {
			this.instructionList.makeFaster();
		}
		return this;
	}

}


