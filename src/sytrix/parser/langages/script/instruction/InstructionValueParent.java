package sytrix.parser.langages.script.instruction;

import sytrix.parser.langages.script.ScriptFunction;
import sytrix.parser.langages.script.ScriptInstruction;
import sytrix.parser.langages.script.ScriptMemory;

public class InstructionValueParent extends InstructionValue {

	private InstructionOperationLog priority;

	public InstructionValueParent(InstructionOperationLog priority) {
		this.priority = priority;
	}

	public Object nextExec(ScriptMemory memory, ScriptFunction function) {
		return this.priority.exec(memory, function);
	}

	@Override
	public String source() {
		return "(" + this.priority.source() + ")";
	}
	
	@Override
	public ScriptInstruction getNextInst() {
		this.priority.makeFaster();
		return this;
	}
}