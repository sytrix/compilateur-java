package sytrix.parser.langages.script;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;


public class ScriptMemory {

	private Map<String, Object> memory;
	private ScriptMemory parentScope;
	
	public ScriptMemory(ScriptMemory parentScope) {
		this.memory = new HashMap<>();
		this.parentScope = parentScope;
	}
	
	private String objToScript(Object obj) {
		String code = null;
		
		if(obj instanceof Integer || obj instanceof Double) {
			code = obj.toString();
		} else if(obj instanceof String) { 
			code = "\"" + obj.toString() + "\"";
		} else if(obj instanceof ScriptFunctionVariable) {
			ScriptFunctionVariable function = (ScriptFunctionVariable)obj;
			code = function.source();
		} else if(obj instanceof LinkedList<?>) {
			@SuppressWarnings("unchecked")
			LinkedList<Object> linkedList = (LinkedList<Object>)obj;
			code = this.linkedListToScript(linkedList);
		}
		
		return code;
	}
	
	private String linkedListToScript(LinkedList<Object> linkedList) {
		StringBuilder result = new StringBuilder();
		Iterator<Object> iter = linkedList.iterator();
		
		result.append('[');
		
		if(iter.hasNext()) {
			Object obj = iter.next();
			result.append(this.objToScript(obj));
		}
		
		while(iter.hasNext()) {
			Object obj = iter.next();
			result.append(',');
			result.append(this.objToScript(obj));
		}
		
		result.append(']');
		
		return result.toString();
	}
	
	public String toScript() {
		Iterator<String> iter = memory.keySet().iterator();
		StringBuilder result = new StringBuilder();
		
		while(iter.hasNext()) {
			String key = iter.next();
			Object obj = memory.get(key);
			String code = this.objToScript(obj);
			
			if(code != null) {
				result.append(key);
				result.append('=');
				result.append(code);
				result.append(';');
			}
		}
		
		return result.toString();
	}

	public Object get(String name) {
		return this.getIfParentExist(name);
	}
	
	private Object getIfParentExist(String name) {
		if(this.memory.containsKey(name)) {
			return memory.get(name);
		}
		if(this.parentScope != null) {
			return this.parentScope.getIfParentExist(name);
		}
		
		return null;
	}
	
	public void putFirstPlace(String name, Object value) {
		memory.put(name, value);
	}

	public void put(String name, Object value) {
		
		if(!this.putIfParentExist(name, value)) {
			memory.put(name, value);
		}
	}
	
	private boolean putIfParentExist(String name, Object value) {
		if(this.memory.containsKey(name)) {
			memory.put(name, value);
			return true;
		}
		if(this.parentScope != null) {
			if(this.parentScope.putIfParentExist(name, value)) {
				return true;
			}
		}
		
		return false;
	}
	
	
}