package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceInteger extends Sequence {
	
	public SequenceInteger() {}
	
	public Cursor find(char[] text, int cursor, int len) {
		StringBuilder str = new StringBuilder();
		int next = cursor;

		while(next < len && (text[next] >= '0' && text[next] <= '9')) {
			str.append(text[next]);
			next++;
		}

		if(next > cursor) {
			Integer val = Integer.parseInt(str.toString());
			return new Cursor(next, ScriptElement.TokenInteger, val);
		}

		return null;
	}
}