package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceSymbole extends Sequence {
	
	private Character[] symboles;

	public SequenceSymbole() {
		this.symboles = new Character[] {
			'(', 
			')', 
			'[', 
			']', 
			'=', 
			',', 
			'.',
			':',
			';',
			'{', 
			'}', 
		};
	}

	public Cursor find(char[] text, int cursor, int len) {
		char t = text[cursor];

		for(Character c : this.symboles) {
			if(c.charValue() == t) {
				return new Cursor(cursor + 1, ScriptElement.Character, c);
			}
		}

		return null;
	}
}