package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceString extends Sequence {
	
	public SequenceString() {}

	public Cursor find(char[] text, int cursor, int len) {
		StringBuilder str = new StringBuilder();
		int next = cursor + 1;
		char first = text[cursor];

		if(first == '"' || first == '\'') {
			boolean escape = false;
			while(next < len && ((text[next] != first) || (text[next] == first && escape))) {
				char c = text[next];
				if(escape) {
					if(c == first || c == '\\') {
						str.append(c);
					}
					escape = false;
				} else {
					if(c == '\\') {
						escape = true;
					} else {
						str.append(c);
					}
				}

				next++;
			}
			next++;
			
			//System.out.println(str.toString());
			
			if(next > len) {
				return null;
			}

			return new Cursor(next, ScriptElement.TokenString, str.toString());
		}

		return null;
	}
}