package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;
import sytrix.parser.langages.script.ScriptOperatorType;

public class SequenceOperatorCmp extends Sequence {
	
	public SequenceOperatorCmp() {}
	
	public Cursor find(char[] text, int cursor, int len) {
		char currChar = text[cursor];
		char nextChar = ' ';

		if(cursor + 1 < len) {
			nextChar = text[cursor + 1];
		}

		if(currChar == '<') {
			if(nextChar == '=') {
				return new Cursor(cursor + 2, ScriptElement.TokenOperatorCmp, ScriptOperatorType.LowerEqual);
			}
			return new Cursor(cursor + 1, ScriptElement.TokenOperatorCmp, ScriptOperatorType.Lower);
		} else if(currChar == '>') {
			if(nextChar == '=') {
				return new Cursor(cursor + 2, ScriptElement.TokenOperatorCmp, ScriptOperatorType.GreaterEqual);
			}
			return new Cursor(cursor + 1, ScriptElement.TokenOperatorCmp, ScriptOperatorType.Greater);
		} else if(currChar == '=') {
			if(nextChar == '=') {
				return new Cursor(cursor + 2, ScriptElement.TokenOperatorCmp, ScriptOperatorType.Equal);
			}
			return null;
		} else if(currChar == '!') {
			if(nextChar == '=') {
				return new Cursor(cursor + 2, ScriptElement.TokenOperatorCmp, ScriptOperatorType.Different);
			}
			return null;
		} 

		return null;
	}
}