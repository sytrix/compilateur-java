package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceDouble extends Sequence {

	public SequenceDouble() {}

	public Cursor find(char[] text, int cursor, int len) {
		StringBuilder str = new StringBuilder();
		int next = cursor;
		boolean findPoint = false;

		while(next < len && ((text[next] >= '0' && text[next] <= '9') || text[next] == '.')) {
			str.append(text[next]);
			if(text[next] == '.') {
				if(findPoint) {
					return null;
				} else {
					findPoint = true;
				}
			}
			next++;
		}

		if(!findPoint) {
			return null;
		}

		if(next > cursor) {
			Double val = Double.parseDouble(str.toString());
			return new Cursor(next, ScriptElement.TokenDouble, val);
		}

		return null;
	}
}