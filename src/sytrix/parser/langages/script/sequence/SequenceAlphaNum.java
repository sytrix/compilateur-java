package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceAlphaNum extends Sequence {

	public SequenceAlphaNum() {}
	
	public Cursor find(char[] text, int cursor, int len) {
		StringBuilder str = new StringBuilder();
		int next = cursor;

		if((text[next] >= 'a' && text[next] <= 'z') || (text[next] >= 'A' && text[next] <= 'Z')) {
			str.append(text[next]);
			next++;
		} else {
			return null;
		}
		
		while(next < len && (
			(text[next] >= 'a' && text[next] <= 'z') || 
			(text[next] >= 'A' && text[next] <= 'Z') ||
			(text[next] >= '0' && text[next] <= '9')
			)) {
			str.append(text[next]);
			next++;
		}

		return new Cursor(next, ScriptElement.TokenAlphaNum, str.toString());
	}
}