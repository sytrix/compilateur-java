package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceOperatorID extends Sequence {
	
	public SequenceOperatorID() {}
	
	public Cursor find(char[] text, int cursor, int len) {
		char currChar = text[cursor];
		char nextChar = ' ';

		if(cursor + 1 < len) {
			nextChar = text[cursor + 1];
		}

		if(currChar == '+' && nextChar == '+') {
			return new Cursor(cursor + 2, ScriptElement.TokenOperatorID, new Character('+'));
		} else if(currChar == '-' && nextChar == '-') {
			return new Cursor(cursor + 2, ScriptElement.TokenOperatorID, new Character('-'));
		}

		return null;
	}
}