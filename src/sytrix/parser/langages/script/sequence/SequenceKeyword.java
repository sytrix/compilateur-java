package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceKeyword extends Sequence {

	public SequenceKeyword() {}
	
	public Cursor find(char[] text, int cursor, int len) {
		StringBuilder str = new StringBuilder();
		int next = cursor;
		
		while(next < len && 
				((text[next] >= 'a' && text[next] <= 'z') 
				|| (text[next] >= 'A' && text[next] <= 'Z')
				|| (text[next] >= '0' && text[next] <= '9'))) {

			str.append(text[next]);
			next++;
		}

		if(next > cursor) {
			String keyword = str.toString();
			if("if".equals(keyword)) {
				return new Cursor(next, ScriptElement.TokenKeywordIf, null);
			} else if("else".equals(keyword)) {
				return new Cursor(next, ScriptElement.TokenKeywordElse, null);
			} else if("for".equals(keyword)) {
				return new Cursor(next, ScriptElement.TokenKeywordFor, null);
			} else if("while".equals(keyword)) {
				return new Cursor(next, ScriptElement.TokenKeywordWhile, null);
			} else if("function".equals(keyword)) {
				return new Cursor(next, ScriptElement.TokenKeywordDefFunction, null);
			}
		}

		return null;
	}
}