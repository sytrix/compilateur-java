package sytrix.parser.langages.script.sequence;

import sytrix.parser.Cursor;
import sytrix.parser.Sequence;
import sytrix.parser.langages.script.ScriptElement;

public class SequenceOperatorAS extends Sequence {
	
	public SequenceOperatorAS() {}
	
	public Cursor find(char[] text, int cursor, int len) {
		
		if(text[cursor] == '+' || text[cursor] == '-') {
			return new Cursor(cursor + 1, ScriptElement.TokenOperatorAS, new Character(text[cursor]));
		} else {
			return null;
		}
	}
}