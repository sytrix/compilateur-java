package sytrix.parser.langages.script;

import sytrix.parser.Group;
import sytrix.parser.GroupRunnable;
import sytrix.parser.Parser;
import sytrix.parser.langages.script.instruction.InstructionAffectation;
import sytrix.parser.langages.script.instruction.InstructionAffectationAS;
import sytrix.parser.langages.script.instruction.InstructionAffectationIndex;
import sytrix.parser.langages.script.instruction.InstructionAffectationMD;
import sytrix.parser.langages.script.instruction.InstructionIndex;
import sytrix.parser.langages.script.instruction.InstructionKeywordDefFunction;
import sytrix.parser.langages.script.instruction.InstructionKeywordFor;
import sytrix.parser.langages.script.instruction.InstructionKeywordIf;
import sytrix.parser.langages.script.instruction.InstructionKeywordWhile;
import sytrix.parser.langages.script.instruction.InstructionList;
import sytrix.parser.langages.script.instruction.InstructionListAlpha;
import sytrix.parser.langages.script.instruction.InstructionMultiple;
import sytrix.parser.langages.script.instruction.InstructionOperationAS;
import sytrix.parser.langages.script.instruction.InstructionOperationCmp;
import sytrix.parser.langages.script.instruction.InstructionOperationLog;
import sytrix.parser.langages.script.instruction.InstructionOperationMD;
import sytrix.parser.langages.script.instruction.InstructionRange;
import sytrix.parser.langages.script.instruction.InstructionValue;
import sytrix.parser.langages.script.instruction.InstructionValueArray;
import sytrix.parser.langages.script.instruction.InstructionValueDouble;
import sytrix.parser.langages.script.instruction.InstructionValueFunction;
import sytrix.parser.langages.script.instruction.InstructionValueIndex;
import sytrix.parser.langages.script.instruction.InstructionValueInteger;
import sytrix.parser.langages.script.instruction.InstructionValueParent;
import sytrix.parser.langages.script.instruction.InstructionValueString;
import sytrix.parser.langages.script.instruction.InstructionValueVariable;
import sytrix.parser.langages.script.instruction.InstructionValueVariableID;
import sytrix.parser.langages.script.sequence.SequenceAlphaNum;
import sytrix.parser.langages.script.sequence.SequenceDouble;
import sytrix.parser.langages.script.sequence.SequenceInteger;
import sytrix.parser.langages.script.sequence.SequenceKeyword;
import sytrix.parser.langages.script.sequence.SequenceOperatorAS;
import sytrix.parser.langages.script.sequence.SequenceOperatorCmp;
import sytrix.parser.langages.script.sequence.SequenceOperatorID;
import sytrix.parser.langages.script.sequence.SequenceOperatorLog;
import sytrix.parser.langages.script.sequence.SequenceOperatorMD;
import sytrix.parser.langages.script.sequence.SequenceString;
import sytrix.parser.langages.script.sequence.SequenceSymbole;

public class ScriptParser extends Parser {

	public ScriptParser() {
		super();

		this.sequences.add(new SequenceKeyword());
		this.sequences.add(new SequenceString());
		this.sequences.add(new SequenceAlphaNum());
		this.sequences.add(new SequenceDouble());
		this.sequences.add(new SequenceInteger());
		this.sequences.add(new SequenceOperatorLog());
		this.sequences.add(new SequenceOperatorCmp());
		this.sequences.add(new SequenceOperatorID());
		this.sequences.add(new SequenceOperatorAS());
		this.sequences.add(new SequenceOperatorMD());
		this.sequences.add(new SequenceSymbole());

		Group grpStart = new Group();
		grpStart.add(new Object[]{ScriptElement.GroupMultiInstruction}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return params[0];
			}
		});
		grpStart.add(new Object[]{ScriptElement.GroupInstruction}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return params[0];
			}
		});
		
		Group grpMultiInstruction = new Group();
		grpMultiInstruction.add(new Object[]{ScriptElement.GroupInstructionClosed, ScriptElement.GroupMultiInstruction}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionMultiple((ScriptInstruction)params[0], (InstructionMultiple)params[1]);
			}
		});
		grpMultiInstruction.add(new Object[]{ScriptElement.GroupInstructionClosed}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionMultiple((ScriptInstruction)params[0], null);
			}
		});

		Group grpInstructionClosed = new Group();
		grpInstructionClosed.add(new Object[]{
			ScriptElement.TokenKeywordIf, new Character('('), ScriptElement.GroupOperatorLog, new Character(')'), 
			new Character('{'), ScriptElement.GroupMultiInstruction, new Character('}'), 
			ScriptElement.TokenKeywordElse, new Character('{'), ScriptElement.GroupMultiInstruction, new Character('}')}, new GroupRunnable() {

			@Override
			public Object run(Object[] params) {
				return new InstructionKeywordIf((InstructionOperationLog)params[2], (InstructionMultiple)params[5], (InstructionMultiple)params[9]);
			}
		});
		grpInstructionClosed.add(new Object[]{
				ScriptElement.TokenKeywordIf, new Character('('), ScriptElement.GroupOperatorLog, new Character(')'), 
			new Character('{'), ScriptElement.GroupMultiInstruction, new Character('}')}, new GroupRunnable() {

			@Override
			public Object run(Object[] params) {
				return new InstructionKeywordIf((InstructionOperationLog)params[2], (InstructionMultiple)params[5], null);
			}
		});
		grpInstructionClosed.add(new Object[]{
				ScriptElement.TokenKeywordFor, new Character('('), 
				ScriptElement.GroupInstruction, new Character(';'), 
				ScriptElement.GroupOperatorLog, new Character(';'), 
				ScriptElement.GroupInstruction, new Character(')'), 
				new Character('{'), ScriptElement.GroupMultiInstruction, new Character('}')}, new GroupRunnable() {

			@Override
			public Object run(Object[] params) {
				return new InstructionKeywordFor(
					(ScriptInstruction)params[2], 
					(InstructionOperationLog)params[4], 
					(ScriptInstruction)params[6], 
					(InstructionMultiple)params[9]);
			}
		});
		grpInstructionClosed.add(new Object[]{
				ScriptElement.TokenKeywordWhile, new Character('('), ScriptElement.GroupOperatorLog, new Character(')'), 
				new Character('{'), ScriptElement.GroupMultiInstruction, new Character('}')}, new GroupRunnable() {

			@Override
			public Object run(Object[] params) {
				return new InstructionKeywordWhile(
					(InstructionOperationLog)params[2], 
					(InstructionMultiple)params[5]);
			}
		});
		grpInstructionClosed.add(new Object[]{ScriptElement.GroupInstruction, new Character(';')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return params[0];
			}
		});

		Group grpInstruction = new Group();
		grpInstruction.add(new Object[]{ScriptElement.GroupAffectationIndex, ScriptElement.TokenOperatorAS, new Character('='), ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionAffectationAS((InstructionAffectationIndex)params[0], (Character)params[1], (InstructionOperationLog)params[3]);
			}
		});
		grpInstruction.add(new Object[]{ScriptElement.GroupAffectationIndex, ScriptElement.TokenOperatorMD, new Character('='), ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionAffectationMD((InstructionAffectationIndex)params[0], (Character)params[1], (InstructionOperationLog)params[3]);
			}
		});
		grpInstruction.add(new Object[]{ScriptElement.GroupAffectationIndex, new Character('='), ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionAffectation((InstructionAffectationIndex)params[0], (InstructionOperationLog)params[2]);
			}
		});
		grpInstruction.add(new Object[]{ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return params[0];
			}
		});
		
		Group grpAffectationIndex = new Group();
		grpAffectationIndex.add(new Object[]{ScriptElement.TokenAlphaNum, ScriptElement.GroupIndex}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionAffectationIndex((String)params[0], (InstructionIndex)params[1]);
			}
		});
		grpAffectationIndex.add(new Object[]{ScriptElement.TokenAlphaNum}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionAffectationIndex((String)params[0], null);
			}
		});
		

		Group grpOperationLog = new Group();
		grpOperationLog.add(new Object[]{ScriptElement.GroupOperatorCmp, ScriptElement.TokenOperatorLog, ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationLog((InstructionOperationCmp)params[0], (Character)params[1], (InstructionOperationLog)params[2]);
			}
		});
		grpOperationLog.add(new Object[]{ScriptElement.GroupOperatorCmp}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationLog((InstructionOperationCmp)params[0], null, null);
			}
		});


		Group grpOperationCmp = new Group();
		grpOperationCmp.add(new Object[]{ScriptElement.GroupOperatorAS, ScriptElement.TokenOperatorCmp, ScriptElement.GroupOperatorCmp}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationCmp((InstructionOperationAS)params[0], (ScriptOperatorType)params[1], (InstructionOperationCmp)params[2]);
			}
		});
		grpOperationCmp.add(new Object[]{ScriptElement.GroupOperatorAS}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationCmp((InstructionOperationAS)params[0], null, null);
			}
		});

		Group grpOperationAS = new Group();
		grpOperationAS.add(new Object[]{ScriptElement.GroupOperatorMD, ScriptElement.TokenOperatorAS, ScriptElement.GroupOperatorAS}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationAS((InstructionOperationMD)params[0], (Character)params[1], (InstructionOperationAS)params[2]);
			}
		});

		grpOperationAS.add(new Object[]{ScriptElement.GroupOperatorMD}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationAS((InstructionOperationMD)params[0], null, null);
			}
		});

		Group grpOperationMD = new Group();
		grpOperationMD.add(new Object[]{ScriptElement.GroupValueIndex, ScriptElement.TokenOperatorMD, ScriptElement.GroupOperatorMD}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationMD((InstructionValue)params[0], (Character)params[1], (InstructionOperationMD)params[2]);
			}
		});
		grpOperationMD.add(new Object[]{ScriptElement.GroupValueIndex}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionOperationMD((InstructionValue)params[0], null, null);
			}
		});

		Group grpValueIndex = new Group();
		grpValueIndex.add(new Object[]{ScriptElement.GroupValue, ScriptElement.GroupIndex}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueIndex((InstructionValue)params[0], (InstructionIndex)params[1]);
			}
		});
		grpValueIndex.add(new Object[]{ScriptElement.GroupValue}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueIndex((InstructionValue)params[0], null);
			}
		});

		Group grpIndex = new Group();
		grpIndex.add(new Object[]{new Character('['), ScriptElement.GroupRange, new Character(']'), ScriptElement.GroupIndex}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionIndex((InstructionRange)params[1], (InstructionIndex)params[3]);
			}
		});
		grpIndex.add(new Object[]{new Character('['), ScriptElement.GroupRange, new Character(']')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionIndex((InstructionRange)params[1], null);
			}
		});

		Group grpRange = new Group();
		grpRange.add(new Object[]{new Character(':'), ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionRange(null, (InstructionOperationLog)params[1], true, false);
			}
		});
		grpRange.add(new Object[]{new Character(':')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionRange(null, null, true, true);
			}
		});
		grpRange.add(new Object[]{ScriptElement.GroupOperatorLog, new Character(':'), ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionRange((InstructionOperationLog)params[0], (InstructionOperationLog)params[2], false, false);
			}
		});
		grpRange.add(new Object[]{ScriptElement.GroupOperatorLog, new Character(':')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionRange((InstructionOperationLog)params[0], null, false, true);
			}
		});
		grpRange.add(new Object[]{ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionRange((InstructionOperationLog)params[0], null, false, false);
			}
		});


		Group grpValue = new Group();
		grpValue.add(new Object[]{new Character('('), ScriptElement.GroupOperatorLog, new Character(')')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueParent((InstructionOperationLog)params[1]);
			}
		});
		grpValue.add(new Object[]{ScriptElement.TokenDouble}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueDouble((Double)params[0]);
			}
		});
		grpValue.add(new Object[]{ScriptElement.TokenInteger}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueInteger((Integer)params[0]);
			}
		});
		grpValue.add(new Object[]{ScriptElement.GroupArray}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return params[0];
			}
		});
		grpValue.add(new Object[]{ScriptElement.GroupFunction}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return params[0];
			}
		});
		grpValue.add(new Object[]{ScriptElement.TokenString}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueString((String)params[0]);
			}
		});
		grpValue.add(new Object[]{ScriptElement.TokenAlphaNum, ScriptElement.TokenOperatorID}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueVariableID((String)params[0], (Character)params[1]);
			}
		});
		grpValue.add(new Object[]{ScriptElement.TokenAlphaNum}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueVariable((String)params[0]);
			}
		});
		grpValue.add(new Object[]{
			ScriptElement.TokenKeywordDefFunction, new Character('('), ScriptElement.GroupListAlpha, new Character(')'), 
			new Character('{'), ScriptElement.GroupMultiInstruction, new Character('}')}, new GroupRunnable() {

			@Override
			public Object run(Object[] params) {
				return new InstructionKeywordDefFunction((InstructionListAlpha)params[2], (InstructionMultiple)params[5]);
			}
		});

		Group grpArray = new Group();
		grpArray.add(new Object[]{new Character('['), new Character(']')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueArray(null);
			}
		});
		grpArray.add(new Object[]{new Character('['), ScriptElement.GroupList, new Character(']')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueArray((InstructionList)params[1]);
			}
		});

		Group grpFunction = new Group();
		grpFunction.add(new Object[]{ScriptElement.TokenAlphaNum, new Character('('), new Character(')')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueFunction((String)params[0], null);
			}
		});
		grpFunction.add(new Object[]{ScriptElement.TokenAlphaNum, new Character('('), ScriptElement.GroupList, new Character(')')}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionValueFunction((String)params[0], (InstructionList)params[2]);
			}
		});

		Group grpList = new Group();
		grpList.add(new Object[]{ScriptElement.GroupOperatorLog, new Character(','), ScriptElement.GroupList}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionList((InstructionOperationLog)params[0], (InstructionList)params[2]);
			}
		});
		grpList.add(new Object[]{ScriptElement.GroupOperatorLog}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionList((InstructionOperationLog)params[0], null);
			}
		});
		
		Group grpListAlpha = new Group();
		grpListAlpha.add(new Object[]{ScriptElement.TokenAlphaNum, new Character(','), ScriptElement.GroupListAlpha}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionListAlpha((String)params[0], (InstructionListAlpha)params[2]);
			}
		});
		grpListAlpha.add(new Object[]{ScriptElement.TokenAlphaNum}, new GroupRunnable() {
			@Override
			public Object run(Object[] params) {
				return new InstructionListAlpha((String)params[0], null);
			}
		});

		
		this.groups.put(ScriptElement.GroupStart, grpStart);
		this.groups.put(ScriptElement.GroupMultiInstruction, grpMultiInstruction);
		this.groups.put(ScriptElement.GroupInstructionClosed, grpInstructionClosed);
		this.groups.put(ScriptElement.GroupInstruction, grpInstruction);
		this.groups.put(ScriptElement.GroupAffectationIndex, grpAffectationIndex);
		this.groups.put(ScriptElement.GroupOperatorLog, grpOperationLog);
		this.groups.put(ScriptElement.GroupOperatorCmp, grpOperationCmp);
		this.groups.put(ScriptElement.GroupOperatorAS, grpOperationAS);
		this.groups.put(ScriptElement.GroupOperatorMD, grpOperationMD);
		this.groups.put(ScriptElement.GroupValueIndex, grpValueIndex);
		this.groups.put(ScriptElement.GroupIndex, grpIndex);
		this.groups.put(ScriptElement.GroupRange, grpRange);
		this.groups.put(ScriptElement.GroupValue, grpValue);
		this.groups.put(ScriptElement.GroupArray, grpArray);
		this.groups.put(ScriptElement.GroupFunction, grpFunction);
		this.groups.put(ScriptElement.GroupList, grpList);
		this.groups.put(ScriptElement.GroupListAlpha, grpListAlpha);

		this.start = ScriptElement.GroupStart;
	}



}