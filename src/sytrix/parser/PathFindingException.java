package sytrix.parser;

public class PathFindingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7704305862332959183L;
	private Token badToken;
	
	public PathFindingException(Token t)
	{
		super();
		this.badToken = t;
	}
	
	public Token getBadToken() {
		return this.badToken;
	}

	
	
}
