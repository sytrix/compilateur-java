package sytrix.parser;

import java.util.Map;

import sytrix.parser.Instruction;

public class Program {

	private Instruction start;

	public Program(Instruction start) {
		this.start = start;
		this.start.makeFaster();
	}
	
	public Object execute(Map<String, Object> bundle) {
		return this.start.first(bundle);
	}
	
	public String getSource() {
		return this.start.source();
	}

}