package sytrix.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.LinkedList;

public class Group {

	private class Grammar {
		private GroupRunnable groupRunnable;
		private Object[] elements;
	}

	private class PathCache {
		private Object element;
		private PathReturn pathReturn;
		private Object value;
		private PathCache(Object element, PathReturn pathReturn, Object value) {
			this.element = element;
			this.pathReturn = pathReturn;
			this.value = value;
		}
	}

	private class PathReturn {
		private GroupExecutable groupExecutable;
		private int nextIndex;
	}

	private List<Grammar> listGrammar;

	public Group() {
		this.listGrammar = new ArrayList<>();
	}

	public void add(Object[] elements, GroupRunnable groupRunnable) {
		Grammar g = new Grammar();

		g.elements = elements;
		g.groupRunnable = groupRunnable;

		this.listGrammar.add(g);
	}

	public GroupExecutable pathfinding(Map<Element, Group> groups, List<Token> tokens) throws Exception {
		PathReturn result = this.pathfinding(groups, tokens, 0);

		if(result != null) {
			if(result.nextIndex == tokens.size()) {
				return result.groupExecutable;
			} else {
				Token badToken = tokens.get(result.nextIndex);
				System.out.println("error : Unexpected token " + result.nextIndex + "/" + tokens.size() + " " + badToken.toString());
				throw new PathFindingException(badToken);
			}
		}

		return null;
	}

	private PathReturn pathfinding(Map<Element, Group> groups, List<Token> tokens, int index) {
		Iterator<Grammar> iter = this.listGrammar.iterator();
		LinkedList<PathCache> listPathCache = new LinkedList<>();
		boolean useCache = true;

		while(iter.hasNext()) {
			Grammar grammar = iter.next();
			Object[] elements = grammar.elements;
			GroupExecutable groupExecutable = new GroupExecutable(grammar.groupRunnable);
			int nbElement = elements.length;
			int currIndex = index;
			boolean pathCacheFucked = false;
			boolean check = true;

			int i = 0;
			while(i < nbElement && check) {
				boolean notInCache = true;

				if(useCache) {
					if(!pathCacheFucked) {
						if(i < listPathCache.size()) {
							PathCache pathCache = listPathCache.get(i);
							if(elements[i] == pathCache.element) {
								if(pathCache.element instanceof Element)
								{
									Element element = (Element)pathCache.element;
									if(element.isGroup()) {
										groupExecutable.addObject(pathCache.pathReturn.groupExecutable);
										currIndex = pathCache.pathReturn.nextIndex;
										notInCache = false;
									} else {
										groupExecutable.addObject(pathCache.value);
										currIndex++;
										notInCache = false;
									}
								} else if(pathCache.element instanceof Character) {
									groupExecutable.addObject(pathCache.value);
									currIndex++;
									notInCache = false;
								}
							} else {
								pathCacheFucked = true;
							}
						} else {
							pathCacheFucked = true;
						}
					}
				}

				if(notInCache) {
					//System.out.println("try:" + elements[i]);
					if(elements[i] instanceof Element) {
						Element element = (Element)elements[i];
						if(element.isGroup()) {
							Group nextGroup = groups.get(element);
							PathReturn pathReturn = nextGroup.pathfinding(groups, tokens, currIndex);
							if(pathReturn == null) {
								check = false;
							} else {
								groupExecutable.addObject(pathReturn.groupExecutable);
								currIndex = pathReturn.nextIndex;

								if(useCache) {
									listPathCache.add(new PathCache(elements[i], pathReturn, null));
								}
							}

						} else {
							if(currIndex < tokens.size()) {
								Token token = tokens.get(currIndex);
								if(element != token.getType()) {
									check = false;
								} else {
									groupExecutable.addObject(token.getValue());
									currIndex++;

									if(useCache) {
										listPathCache.add(new PathCache(elements[i], null, token.getValue()));
									}
								}
							} else {
								check = false;
							}
						}
					} else if(elements[i] instanceof Character) {
						if(currIndex < tokens.size()) {
							Token token = tokens.get(currIndex);
							if(token.getType().isChar()) {
								Character character = (Character)elements[i];
								if(token.getValue().equals(character)) {
									groupExecutable.addObject(token.getValue());
									currIndex++;

									if(useCache) {
										listPathCache.add(new PathCache(elements[i], null, token.getValue()));
									}
								} else {
									check = false;
								}
							} else {
								check = false;
							}
						} else {
							check = false;
						}
					} else {
						check = false;
						System.out.println("erreur : element inconnu");
						return null;
					}
				}
				i++;
			}

			if(check) {
				PathReturn result = new PathReturn();
				result.groupExecutable = groupExecutable;
				result.nextIndex = currIndex;
				return result;
			}
			

		}

		return null;
	}
}