package sytrix.parser;

import java.util.Map;

public abstract class Instruction {
	
	public abstract Object first(Map<String, Object> bundle);
	public abstract void makeFaster();
	public abstract String source();
	
}
