package sytrix.parser;

abstract public class Sequence {

	/*
	check string sequences 
	result: 
		return null if the sequences is not recognized by the parser
		return !=null if the cursor has moved to another position
	*/

	abstract public Cursor find(char[] text, int cursor, int len);
}