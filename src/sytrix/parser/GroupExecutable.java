package sytrix.parser;

import java.util.List;
import java.util.ArrayList;

public class GroupExecutable {
	private List<Object> listObj;
	private GroupRunnable runnable;

	public GroupExecutable(GroupRunnable runnable) {
		this.listObj = new ArrayList<>();
		this.runnable = runnable;
	}

	public void addObject(Object obj) {
		this.listObj.add(obj);
	}

	public Object exec() {
		int nbObj = this.listObj.size();
		Object[] params = new Object[nbObj];

		for(int i = 0; i < nbObj; i++) {
			Object obj = this.listObj.get(i);
			if(obj instanceof GroupExecutable) {
				GroupExecutable executable = (GroupExecutable)obj;
				params[i] = executable.exec();
			} else {
				params[i] = obj;
			}
		}

		return this.runnable.run(params);
	}
}

