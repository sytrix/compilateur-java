package sytrix.parser;


public class Cursor {
	private int next;
	private Element type;
	private Object value;

	public Cursor(int next, Element type, Object value) {
		this.next = next;
		this.type = type;
		this.value = value;
	}

	public int getNext() {
		return this.next;
	}

	public Element getType() {
		return this.type;
	}

	public Object getValue() {
		return this.value;
	}
}
