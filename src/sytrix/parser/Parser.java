package sytrix.parser;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import sytrix.parser.langages.script.ScriptInstruction;

import java.util.HashMap;

abstract public class Parser {

	protected List<Sequence> sequences;
	protected Map<Element, Group> groups;
	protected Element start;

	public Parser() {
		this.sequences = new ArrayList<>();
		this.groups = new HashMap<>();
		this.start = null;
	}


	public Program compile(String text) throws Exception {
		if(this.start == null) {
			System.err.println("start point is not configured");
			return null;
		}

		//System.out.println("tokenize");

		List<Token> tokens = this.findToken(text);
		
		/*
		
		for(Token t : tokens) {
			System.out.println("debug:" + t.getType().toString() + "\t\t val:" + t.getValue() + "\t\t beg:" + t.getIdxBeg() + "\t\t end:" + t.getIdxEnd());
		}
		*/

		//System.out.println("path finding");

		GroupExecutable groupExecutable = this.groups.get(this.start).pathfinding(this.groups, tokens);
		
		if(groupExecutable != null) {
			//System.out.println("execution");

			Object start = groupExecutable.exec();

			//System.out.println("program");

			return new Program((ScriptInstruction)start);
		}

		return null;
	}

	private List<Token> findToken(String text) {
		List<Token> result = new ArrayList<Token>();
		int currIndex = 0;
		int len = text.length();
		int nbSeq = this.sequences.size();
		char[] chars = text.toCharArray();

		while(currIndex < len) { 
			int i = 0;
			boolean search = true;

			while(i < nbSeq && search) {
				Sequence s = this.sequences.get(i);
				Cursor cursor = s.find(chars, currIndex, len);

				if(cursor != null) {
					int nextIndex = cursor.getNext();
					Token token = new Token(cursor.getType(), cursor.getValue(), currIndex, nextIndex);
					result.add(token);

					search = false;
					currIndex = nextIndex;
				}

				i++;
			}

			if(search) {
				currIndex++;
			}
		}

		return result;
	}

}
