package sytrix.parser;

public class Token {
	private Element type;
	private Object value;
	private int idxBeg;
	private int idxEnd;

	public Token(Element type, Object value, int idxBegin, int idxEnd) {
		this.type = type;
		this.value = value;
		this.idxBeg = idxBegin;
		this.idxEnd = idxEnd;
	}
	
	@Override
	public String toString() {
		return "Token [type=" + type + ", value='" + value + "', idxBeg=" + idxBeg + ", idxEnd=" + idxEnd + "]";
	}

	public Element getType() {
		return this.type;
	}

	public Object getValue() {
		return this.value;
	}

	public int getIdxBeg() {
		return idxBeg;
	}

	public int getIdxEnd() {
		return idxEnd;
	}

	public int getLength() {
		return idxEnd - idxBeg;
	}
}